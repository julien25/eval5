# Évaluation compétence 1 lot 2
L'objectif est de réaliser un diagramme de classe ainsi qu'un diagramme de séquence d'après les users stories définies dans le cahier des charges.  
L'application à développer est un suivi de popularité de blagues sur Chuck Norris.

## Objectifs

- Savoir concevoir d'après les diagrammes UML ;
- Mettre en place une base de conception efficaces ;
- Créer une maquette

## Réalisation

- Durée : **Vendredi 09 août 2019 9h00 - vendredi 09 août 2019 17h00**. Tout retard ne sera pas corrigé.
- Groupe : **En solo**

## Contraintes

- Pas d'éléments externes

## Rendu

- **Un diagramme de classe**
- **Un diagramme de séquence**
- **La maquette de la page d'accueil** dans les résolutions 360px, 800px et 1200px (il y a donc 3 maquettes à réaliser).


## Spécifications fonctionnelles

### Partie 1 : les blagues

- Les blagues sont classées en catégories elle même divisées en sous catégories :
    - *life- contenant : animal, food ;
    - *human- contenant : career, celebrity, dev, history, political, religion, science ;
    - *other- contenant : explicit, money ;
    - *hobies- contenant : fashion, movie, music, sport, travel.
- Chaque catégorie de premier niveau contient une couleur.
- Une blague est peut être dans une seule catégorie enfante. 
- Une blague peut ne pas être catégorisée.
- Les catégories parentes n'ont pas de blague directement associé.
- Une blague contient un identifiant, une date et un contenu (la blague). Vous pouvez vous référer directement à l'[API Chuck Norris](https://api.chucknorris.io/jokes/random).
- Il est possible de connaitre les votes des jurés sur chaque blague ainsi que la moyenne d'appréciation (nombre de votes positifs/nombre de votes négatifs).

### Partie 2 : le jury
- Chaque membre du jury peut voter pour ou contre une blague (chacun ses goûts que voulez vous).
- Un juré est défini par un prénom, un nom et un avatar.

## Écrans

### Architecture

Chaque page est divisée en 3 parties :

#### le header

Il contient

- un logo qui permettra à l'utilisateur de retourner sur la page d'accueil,
- un lien vers la liste des blagues,
- un lien vers la page 'Ajouter une blague'.

#### le contenu

Se référer à la partie **Pages**.

#### le footer
Il contient :
 
- l'année en cours (celle-ci devra être dynamique),
- le copyright,
- le lien vers [API Chuck Norris](https://api.chucknorris.io),
- la mention 'Réalisé par Simplon Rodez'.

### Pages

#### Accueil

L'accueil est divisée en 3 parties :

- Une section de bienvenue contenant une des blagues sélectionnée au hasard, le résultat de son vote, un lien vers celle-ci ainsi qu'un bouton 'Ajouter une nouvelle blague' permettant à l'utilisateur de se diriger vers la page 'Ajouter une blague'.
- Une section 'blagues' listant les cinq blagues ayant eu le meilleur vote (chaque blague est accompagné de son vote et d'un lien vers sa page). Un bouton 'Voir toutes les blagues' permettra d'aller sur la page 'Liste des blagues'.
- Une section 'Jury' listant tous les membres du jury avec, pour chacun : l'avatar, le nom, le prénom, le pourcentage de vote positif ainsi qu'un lien vers sa page personnelle.

#### Liste des blagues

La page contient :

- le titre 'Toutes les chuckin blagues',
- la même liste que celle de la page d'accueil mais contenant toutes les blagues listées par chronologie inversée (de la plus récente à la plus ancienne) classées par catégorie.

#### Ajout de blague

La page contient :

- le titre 'Ajouter une blague' ;
- un bouton : 'Chuck it!' ;
- une fois le bouton cliqué, celui-ci est remplacé par la blague téléchargée via l'API, sa catégorie (si présente) et sa date de création ;
- une section vote total (grisée tant que le dernier juré n'a pas voté) contenant une progressbar avec un texte de résultat ainsi qu'un gif animé provenant de l'[API yesNo](https://yesno.wtf) en fond ;
- une section juré contenant la liste de chaque juré avec, pour chacun :
    - l'avatar,
    - le prénom et le nom,
    - un bouton pour un vote positif,
    - un bouton pour un vote négatif,
    - un lien vers la page du juré

#### Edition d'une blague

La page contient les mêmes éléments que la page 'ajout' lorsqu'elle est à son état final.  

#### Edition d'un jury

La page contient :

- l'avatar du juré,
- le prénom et le nom du juré (titre de la page)
- le pourcentage de vote positif sous forme de progressbar
- une section 'prénom-juré aime' (par exemple 'John aime') contenant la liste des blagues (même apparence que celle de la page d'accueil) que le juré a aimé et ordonnée de façon chronologique inversée (de la plus récente à la plus ancienne),
- une section 'prénom-juré n'aime pas' (par exemple 'John n'aime pas') contenant la liste des blagues (même apparence que celle de la page d'accueil) que le juré n'a pas aimé et ordonnée de façon chronologique inversée (de la plus récente à la plus ancienne).

## Critères de validation

### Compétence 1 : Maquetter une application

- La maquette prend en compte les spécificités fonctionnelles décrites dans les cas d'utilisation ou les scénarios utilisateur
- L'enchaînement des écrans est formalisé par un schéma
- La maquette et l'enchaînement des écrans sont validés par l’utilisateur
- La maquette respecte la charte graphique de l’entreprise
- La maquette est conforme à l'expérience utilisateur et à l’équipement ciblé
- La maquette respecte les principes de sécurisation d’une interface utilisateur
- La maquette prend en compte les exigences de sécurité spécifiques de l’application