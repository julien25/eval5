-- Jury
INSERT INTO jury (firstname, lastname, avatar) VALUES
('Julien', 'Nguyen', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Rodrigue', 'Queraud', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Florent', 'Serres', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Jonathan', 'Tapin', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Benoît', 'Joffre', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Rabe', 'Hama', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Romain', 'Carmier', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Gilles', 'Ries', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Pauline', 'Fauche', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Serge', 'Aguerra', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Olivier', 'Sebert', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Christophe', 'Clares', NULL);

INSERT INTO jury (firstname, lastname, avatar) VALUES
('Jacquie', 'Michel', NULL);

-- Category

INSERT INTO category (name) VALUES
('Drôle');

INSERT INTO category (name) VALUES
('Moyennement Drôle');

INSERT INTO category (name) VALUES
('Pas Drôle');

-- Joke

INSERT INTO joke (content, date, category_id) VALUES
('tu dis ça parce que tu es en colère', 28/10/94, '1');

INSERT INTO joke (content, date, category_id) VALUES
('Quelle mamie fait peur aux voleurs ? Mamie Traillette', 28/10/94, '1');

INSERT INTO joke (content, date, category_id) VALUES
('J ai une blague sur les magasins Mais elle a pas supermarché', 28/10/94, '2');

INSERT INTO joke (content, date, category_id) VALUES
('Pourquoi est-ce c est difficile de conduire dans le Nord ? Parce que les voitures arrêtent PAS DE CALER', 28/10/94, '2');

INSERT INTO joke (content, date, category_id) VALUES
('Pourquoi est-ce que les mexicains mangent-ils aux toilettes ? Parce qu ils aiment manger épicé', 28/10/94, '1');

INSERT INTO joke (content, date, category_id) VALUES
('Quel est le bar préféré des espagnols ? Le Bar-celone', 28/10/94, '3');

INSERT INTO joke (content, date, category_id) VALUES
(' Qu est-ce qu un tennisman adore faire ? Rendre des services', 28/10/94, '2');

INSERT INTO joke (content, date, category_id) VALUES
('Que se passe-t-il quand 2 poissons s énervent ? Le thon monte', 28/10/94, '2');

INSERT INTO joke (content, date, category_id) VALUES
('Quel fruit est assez fort pour couper des arbres? Le ci-tron', 28/10/94, '3');

INSERT INTO joke (content, date, category_id) VALUES
('Que dit une imprimante dans l eau ? J ai papier', 28/10/94, '1');



